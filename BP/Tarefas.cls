Class ACSC.Negocios.Assistencial.BancoSangue.BP.Tarefas
{

Method OnTask() As %Status
{
 
  SET tSC = $$$OK
 
  try {
  
  	SET tStartTime = $ZDATETIME($HOROLOG,3,3)
	SET tSC = ##Class(Ens.Director).CreateBusinessService("GeraArquivo",.pBS)
	SET tSC = pBS.CarregaArquivo()
			
	KILL pBS		
  
  } catch  e {
		SET tSC = $System.Status.GetErrorText(e)
  }
}

}
