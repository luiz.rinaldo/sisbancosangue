/// 
Class ACSC.Negocios.Assistencial.BancoSangue.BP.GeraArquivo Extends Ens.BusinessProcessBPL
{

Storage Default
{
<Type>%Library.CacheStorage</Type>
}

/// BPL Definition
XData BPL [ XMLNamespace = "http://www.intersystems.com/bpl" ]
{
<process language='objectscript' request='ACSC.Negocios.Assistencial.BancoSangue.BP.Request' response='ACSC.Negocios.Assistencial.BancoSangue.BP.Response' height='2000' width='2000' >
<context>
<property name='PacientesInternados' type='%Stream.GlobalCharacter' instantiate='0' />
<property name='NomeArquivo' type='%String' initialexpression='"SbsPac.txt"' instantiate='0' >
<parameters>
<parameter name='MAXLEN'  value='' />
</parameters>
</property>
<property name='DeletarArquivo' type='%Boolean' initialexpression='1' instantiate='0' />
<property name='CaminhoArquivo' type='%String' initialexpression='"\home\confluence"' instantiate='0' >
<parameters>
<parameter name='MAXLEN'  value='' />
</parameters>
</property>
</context>
<sequence xend='200' yend='550' >
<call name='ConsultarBase' target='MVSOUL CORP SQL Connector' async='0' xpos='200' ypos='250' >
<request type='ACSC.Negocios.Assistencial.BancoSangue.BO.Consulta.Request' />
<response type='ACSC.Negocios.Assistencial.BancoSangue.BO.Consulta.Response' >
<assign property="context.PacientesInternados" value="callresponse.PacienteInternado" action="set" />
<assign property="response.Status" value="callresponse.Status" action="set" />
</response>
</call>
<call name='Gerar Arquivo' target='MVSOUL CORP FILE Connector' async='0' xpos='200' ypos='350' >
<request type='ACSC.Negocios.Adaptadores.FILE.Request' >
<assign property="callrequest.Texto" value="context.PacientesInternados" action="set" />
<assign property="callrequest.NomeArquivo" value="context.NomeArquivo" action="set" />
<assign property="callrequest.DeletarArquivo" value="context.DeletarArquivo" action="set" />
<assign property="callrequest.CaminhoArquivo" value="context.CaminhoArquivo" action="set" />
</request>
<response type='ACSC.Negocios.Adaptadores.FILE.Response' >
<assign property="response.Status" value="callresponse.Status" action="set" />
</response>
</call>
<call name='GerarArquivo' target='MVSoul BancoSangue Grv Aquivo' async='0' xpos='200' ypos='450' disabled="true">
<request type='ACSC.Negocios.Assistencial.BancoSangue.BO.Gravar.Request' >
<assign property="callrequest.PacienteInternado" value="context.PacientesInternados" action="set" />
</request>
<response type='ACSC.Negocios.Assistencial.BancoSangue.BO.Gravar.Response' >
<assign property="response.Status" value="callresponse.Status" action="set" />
</response>
</call>
</sequence>
</process>
}

}
