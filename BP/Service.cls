Class ACSC.Negocios.Assistencial.BancoSangue.BP.Service Extends Ens.BusinessService
{

Method CarregaArquivo() As %Status
{
	
	try {
		set tRequest = ""
		Set tSC=..ProcessInput(tRequest,.tResponse)
	} catch e {
		SET tSC = $System.Status.GetErrorText(e)
	} 
	Quit tSC
}

Method OnProcessInput(pInput As %RegisteredObject, Output pOutput As %RegisteredObject) As %Status
{
	try {
		Set tSC = ..SendRequestSync("CarregaArquivo",pInput,.pOutput)
	} catch e {
		SET tSC = $System.Status.GetErrorText(e)
	} 	
	Quit tSC
}

}
