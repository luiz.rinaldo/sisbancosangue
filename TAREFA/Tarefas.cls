Class ACSC.Negocios.Assistencial.BancoSangue.TAREFA.Tarefas Extends %SYS.Task.Definition
{

Method OnTask() As %Status
{
 
  SET tSC = $$$OK 
  TRY 
  {  	
	SET tSC = ##Class(Ens.Director).CreateBusinessService("ACSC.Negocios.Assistencial.BancoSangue.BS.Service",.pBS)
	SET tSC = pBS.CarregaArquivo()			
	KILL pBS  
  } 
  CATCH(e) 
  {
	SET tSC = e.AsStatus()
  }
  QUIT tSC
}

}
