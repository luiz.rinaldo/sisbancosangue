Class ACSC.Negocios.Assistencial.BancoSangue.BS.Service Extends Ens.BusinessService
{

Method CarregaArquivo() As %Status
{
	
	try {
		set tRequest = ""
		Set tSC=..ProcessInput(tRequest,.tResponse)
	} catch e {
		SET tSC = $System.Status.GetErrorText(e)
	} 
	Quit tSC
}

Method OnProcessInput(pInput As %RegisteredObject, Output pOutput As %RegisteredObject) As %Status
{
	try {
		Set tSC = ..SendRequestSync("Gera Dados para Banco de Sangue",pInput,.pOutput)
	} catch e {
		SET tSC = $System.Status.GetErrorText(e)
	} 	
	Quit tSC
}

}
