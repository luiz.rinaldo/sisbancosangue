Class ACSC.Negocios.Assistencial.BancoSangue.BO.Gravar.Operation Extends Ens.BusinessOperation
{

Parameter ADAPTER = "EnsLib.File.OutboundAdapter";

Property Adapter As EnsLib.File.OutboundAdapter;

Parameter INVOCATION = "Queue";

Method GravarArquivo(pRequest As ACSC.Negocios.Assistencial.BancoSangue.BO.Gravar.Request, Output pResponse As ACSC.Negocios.Assistencial.BancoSangue.BO.Gravar.Response) As %Status
{
	SET tSC = $System.Status.OK()
	HANG 40 BREAK
	//file=##class(%File).%New()	
	//SET ..Adapter.FilePath="\home\confluence"
	//SET ..Adapter.FilePath="\\10.4.25.18\transf"
	SET ..Adapter.FilePath="\transf"
	try {
	SET pResponse = ##Class(ACSC.Negocios.Assistencial.BancoSangue.BO.Gravar.Response).%New()
	IF (..Adapter.Exists("SbsPac.txt")) {
	    //SET tSC=..Adapte.Delete("SbsPac.txt")
	    SET tSC=..Adapter.Delete("SbsPac.txt")
	    //file.Delete("SbsPac.txt")
	}	
		SET tSC=..Adapter.PutLine("SbsPac.txt",pRequest.PacienteInternado.Read())
	    //set tSC=..Adapter.Overwrite = 1
	} catch e {
		SET tSC = $System.Status.GetErrorText(e)
	}
	SET pResponse.Status = tSC 
	Quit tSC
}

XData MessageMap
{
<MapItems>
    <MapItem MessageType="ACSC.Negocios.Assistencial.BancoSangue.BO.Gravar.Request">
        <Method>GravarArquivo</Method>
    </MapItem>
</MapItems>
}

}
