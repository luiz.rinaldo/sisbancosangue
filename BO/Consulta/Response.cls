Class ACSC.Negocios.Assistencial.BancoSangue.BO.Consulta.Response Extends Ens.Response
{

Property Status As %Status;

Property PacienteInternado As %Stream.GlobalCharacter;


Storage Default
{
<Data name="ResponseDefaultData">
<Subscript>"Response"</Subscript>
<Value name="1">
<Value>Status</Value>
</Value>
<Value name="2">
<Value>PacienteInternado</Value>
</Value>
</Data>
<DefaultData>ResponseDefaultData</DefaultData>
<Type>%Library.CacheStorage</Type>
}

}
